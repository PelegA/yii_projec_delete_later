<?php
namespace app\controllers;
//השארתי את זה לבינתיים אחכ אני אשנה
use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$user = $auth->createRole('user');
		$auth->add($user);
		
		$user1 = $auth->createRole('user1');
		$auth->add($user1);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionPermissions()
	{
		//define all permissions together- for auth_item table
		$auth = Yii::$app->authManager;
		
		$suggestActivity = $auth->createPermission('suggestActivity');
		$suggestActivity->description = 'user can suggest activity';
		$auth->add($suggestActivity);
		
		$updateOwnActivity = $auth->createPermission('updateOwnActivity');
		$updateOwnActivity->description = 'user1 can update own activity';
		$auth->add($updateOwnActivity);
		
		$updateActivity = $auth->createPermission('updateActivity');
		$updateActivity->description = 'user1 can update activity';
		$auth->add($updateActivity);
		
		$upgradeUser = $auth->createPermission('upgradeUser');
		$upgradeUser->description = 'upgrade user';
		$auth->add($upgradeUser);

		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Every user can update his/her
									own profile ';
		$auth->add($updateUser);
		 
		$deleteUser = $auth->createPermission('deleteUser');
		$deleteUser->description = 'Admin can delete users ';
		$auth->add($deleteUser); 
		 
		$deleteActivity  = $auth->createPermission('deleteActivity');
		$deleteActivity->description = 'Admin can delete activities';
		$auth->add($deleteActivity);		
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		//user
		$user = $auth->getRole('user');
		
		$suggestActivity = $auth->getPermission('suggestActivity');
		$auth->addChild($user, $suggestActivity);
		
		//user1
		$user1 = $auth->getRole('user1');
		$auth->addChild($user1, $user);
		
		$updateOwnActivity = $auth->getPermission('updateOwnActivity');
		$auth->addChild($user1, $updateOwnActivity);
		
		//admin
		$admin = $auth->getRole('admin');
		$auth->addChild($admin, $user1);
		
		$updateActivity = $auth->getPermission('updateActivity');
		$auth->addChild($admin, $updateActivity);

		$upgradeUser = $auth->getPermission('upgradeUser');
		$auth->addChild($admin, $upgradeUser);		
		$deleteUser = $auth->getPermission('deleteUser');
		$auth->addChild($admin, $deleteUser);	
		
		//$updateUser = $auth->getRole('updateUser');
		//$auth->addChild($admin, $updateUser);
		
		//$deleteActivity = $auth->getRole('deleteActivity');
		//$auth->addChild($admin, $deleteActivity);		

		
		
	}
}